const images = document.querySelectorAll(".image-to-show");
let currentIndex = 0;

function showImage(index) {
    images.forEach((image, i) => {
        if (i === index) {
            image.style.display = "block";
        } else {
            image.style.display = "none";
        }
    });
}

function startSlideshow() {
    currentIndex = (currentIndex + 1) % images.length;
    showImage(currentIndex);
}

setInterval(startSlideshow, 3000); // Автоматичний показ кожні 3 секунди
